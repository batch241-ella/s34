const express = require("express");
const app = express(); 
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

let users = [];

app.post("/add-user", (request, response) => {
    if(request.body.username && request.body.password){
        users.push(request.body);
        response.send(`User ${request.body.username} successfully registered!`);
    } else {
        response.send("Please input BOTH username and password.");
    }
});


// ACTIVITY
app.get('/home', (request, response) => {
	response.send("Welcome to the home page");
});

app.get('/users', (request, response) => {
	response.send(users);
});

app.delete('/delete-user', (request, response) => {
	let message = "Database is empty, user does not exist";
	for (let i = 0; i < users.length; i++) {
		if (users[i].username == request.body.username) {
			users.splice(i,1);
			message = `User ${request.body.username} has been deleted.`;
			break;
		} else {
			message = "User does not exist";
		}
	}
	response.send(message);
});


// Tells our server to listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`));